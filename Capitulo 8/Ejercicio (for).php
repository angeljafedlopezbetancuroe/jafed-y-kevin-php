<html>

<head>
  <title>Tabla del 2 con for</title>
</head>

<body>

  <?php

  echo "Tabla de multiplicar del 2";
  echo "<br>";
  for ($f = 2; $f <= 20; $f = $f + 2) {
    echo $f;
    echo "-";
  }
  ?>

</body>

</html>