<html>

<head>
  <title>Tabla del 2 con do while</title>
</head>

<body>

  <?php


  echo "Tabla de multiplicar del 2";
  echo "<br>";
  $f = 2;
  do {
    echo $f;
    echo "-";
    $f = $f + 2;
  } while ($f <= 20);

  ?>

</body>

</html>