<html>

<head>
  <title>tabla del 2 con el while</title>
</head>

<body>

  <?php

  echo "Tabla de multiplicar del 2";
  echo "<br>";
  $f = 2;
  while ($f <= 20) {
    echo $f;
    echo "-";
    $f = $f + 2;
  }
  ?>

</body>

</html>