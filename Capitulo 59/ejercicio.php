<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aumento de precios</title>
</head>

<body>

  <?php
  $mysql = new mysqli("localhost", "root", "", "base1");
  if ($mysql->connect_error)
    die("Problemas con la conexión a la base de datos");

  $mysql->query("update articulos set precio=precio*1.10 where precio<=5") or
    die($mysql->error);

  echo 'La cantidad de artículos que aumentaron su precio es:';
  echo $mysql->affected_rows;

  $mysql->close();
  ?>

</body>

</html>